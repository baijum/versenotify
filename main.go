package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/codegangsta/negroni"
	"github.com/garyburd/redigo/redis"
	"github.com/gorilla/mux"
)

type TestVerse struct {
	BookName        string `json:"book_name"`
	BookId          string `json:"book_id"`
	BookOrder       string `json:"book_order"`
	ChapterId       string `json:"chapter_id"`
	ChapterTitle    string `json:"chapter_title"`
	VerseId         string `json:"verse_id"`
	VerseText       string `json:"verse_text"`
	ParagraphNumber string `json:"paragraph_number"`
}

var OTBooks = map[string]string{
	"Gen":   "Genesis",
	"Exod":  "Exodus",
	"Lev":   "Leviticus",
	"Num":   "Numbers",
	"Deut":  "Deuteronomy",
	"Josh":  "Joshua",
	"Judg":  "Judges",
	"Ruth":  "Ruth",
	"1Sam":  "1 Samuel",
	"2Sam":  "2 Samuel",
	"1Kgs":  "1 Kings",
	"2Kgs":  "2 Kings",
	"1Chr":  "1 Chronicles",
	"2Chr":  "2 Chronicles",
	"Ezra":  "Ezra",
	"Neh":   "Nehemiah",
	"Esth":  "Esther",
	"Job":   "Job",
	"Ps":    "Psalms",
	"Prov":  "Proverbs",
	"Eccl":  "Ecclesiastes",
	"Song":  "Song of Solomon",
	"Isa":   "Isaiah",
	"Jer":   "Jeremiah",
	"Lam":   "Lamentations",
	"Ezek":  "Ezekiel",
	"Dan":   "Daniel",
	"Hos":   "Hosea",
	"Joel":  "Joel",
	"Amos":  "Amos",
	"Obad":  "Obadiah",
	"Jonah": "Jonah",
	"Mic":   "Micah",
	"Nah":   "Nahum",
	"Hab":   "Habakkuk",
	"Zeph":  "Zephaniah",
	"Hag":   "Haggai",
	"Zech":  "Zechariah",
	"Mal":   "Malachi",
}
var NTBooks = map[string]string{
	"Matt":   "Matthew",
	"Mark":   "Mark",
	"Luke":   "Luke",
	"John":   "John",
	"Acts":   "Acts",
	"Rom":    "Romans",
	"1Cor":   "1 Corinthians",
	"2Cor":   "2 Corinthians",
	"Gal":    "Galatians",
	"Eph":    "Ephesians",
	"Phil":   "Philippians",
	"Col":    "Colossians",
	"1Thess": "1 Thessalonians",
	"2Thess": "2 Thessalonians",
	"1Tim":   "1 Timothy",
	"2Tim":   "2 Timothy",
	"Titus":  "Titus",
	"Phlm":   "Philemon",
	"Heb":    "Hebrews",
	"Jas":    "James",
	"1Pet":   "1 Peter",
	"2Pet":   "2 Peter",
	"1John":  "1 John",
	"2John":  "2 John",
	"3John":  "3 John",
	"Jude":   "Jude",
	"Rev":    "Revelation",
}

func VerseHandler(w http.ResponseWriter, r *http.Request) {
	translation := r.FormValue("translation")
	book := r.FormValue("book")
	chapter := r.FormValue("chapter")
	verse := r.FormValue("verse")

	var vl []TestVerse

	dam := "ENG%sN2ET"

	if _, ok := OTBooks[book]; ok {
		dam = "ENG%sO2ET"
	}

	dam = fmt.Sprintf(dam, translation)

	dbtKey := os.Getenv("DBT_KEY")

	apiBase := fmt.Sprintf("http://dbt.io/text/verse?key=%s&v=2", dbtKey)

	key := fmt.Sprintf("&dam_id=%s&book_id=%s&chapter_id=%s&verse_start=%s", dam, book, chapter, verse)

	vt, err := redis.String(RC.Do("GET", key))

	if err != nil {
		log.Println(err)
	}

	if vt == "" {
		log.Println("Key doesn't exist")
		apiFull := apiBase + key

		resp, _ := http.Get(apiFull)
		defer resp.Body.Close()

		decoder := json.NewDecoder(resp.Body)

		err = decoder.Decode(&vl)

		if err != nil {
			log.Fatal("Unable to decode body", err)
		}

		v := vl[0]
		vt = v.VerseText

		RC.Do("SET", key, vt)
	}

	RC.Do("EXPIRE", key, 7*24*60*60)

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(vt))
}

var RC redis.Conn

func openRedisConn() {
	var err error
	ip := os.Getenv("REDIS_PORT_6379_TCP_ADDR")
	port := os.Getenv("REDIS_PORT_6379_TCP_PORT")
	url := fmt.Sprintf("%s:%s", ip, port)
	RC, err = redis.Dial("tcp", url)

	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	openRedisConn()
	defer RC.Close()

	r := mux.NewRouter()
	r.HandleFunc("/api/v1/verse", VerseHandler).Methods("GET")
	n := negroni.Classic()
	n.UseHandler(r)
	n.Run("0.0.0.0:3000")
}
