# Notify Bible Verse

## URL Design

- http://versenotify.com/#/John3:16?translation=KJV

## TODO

- Browser level cache for verses in Localstorage

## Running docker container

docker run -d --name redis -v /var/redis:/data redis
docker run -d --name web -v /root/versenotify:/versenotify -p 3000:3000 -e "DBT_KEY=secretkey" --link redis:redis ubuntu:14.04 /versenotify/run.sh
