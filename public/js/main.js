function onShowNotification() {
    console.log('notification is shown!');
}

function onCloseNotification() {
    console.log('notification is closed!');
}

function onClickNotification() {
    console.log('notification was clicked!');
}

function onErrorNotification() {
    console.error('Error showing notification. You may need to request permission.');
}

function onPermissionGranted() {
    console.log('Permission has been granted by the user');
    doNotification(true);
}

function onPermissionDenied() {
    console.warn('Permission has been denied by the user');
}

function doNotification(newrequest) {
    var book = document.getElementById("notify-book");
    var bookKey = book.value;
    var bookText = book.options[book.selectedIndex].text;
    var chapter = document.getElementById("notify-chapter");
    var chapterKey = chapter.value;
    var verse = document.getElementById("notify-verse");
    var verseKey = verse.value;
    var translation = document.getElementById("notify-translation");
    var translationKey = translation.value;
    if (newrequest == true) {
	xmlhttp = new XMLHttpRequest();
	apiFull = "/api/v1/verse" + "?book=" + bookKey + "&chapter=" + chapterKey + "&verse=" + verseKey + "&translation=" + translationKey;
	fullVerseKey = bookText + " " + chapterKey + ":" + verseKey;
	xmlhttp.open("GET", apiFull, true);
	xmlhttp.send();
	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		var verseText = xmlhttp.responseText;

		var myNotification = new Notify(fullVerseKey, {
		    body: verseText,
		    tag: 'Bible Verse',
		    notifyShow: onShowNotification,
		    notifyClose: onCloseNotification,
		    notifyClick: onClickNotification,
		    notifyError: onErrorNotification,
		    timeout: 14
		});
		myNotification.show();
		window.myNotification = myNotification;
	    }
	}
    } else {
	window.myNotification.show();
    }
}

App = Ember.Application.create();

// Routes
App.Router.map(function() {
    this.resource('verse', { path: ':verse_key' });
    this.route('about');
});

App.IndexRoute = Ember.Route.extend({
});

App.BookSelectionComponent = Ember.Component.extend({
    classNames: ['col-sm-8'],
    names: ["Yehuda", "Tom"],
});

App.VerseSelectionComponent = Ember.Component.extend({
    actions: {
	notify: function() {
	    if (Notify.needsPermission) {
		Notify.requestPermission(onPermissionGranted, onPermissionDenied);
	    } else {
		doNotification(true);
	    }
	    var interval = document.getElementById("notify-interval");
	    var intervalKey = interval.value;
	    var intervalId = setInterval("doNotification(false)", 1000*60*parseInt(intervalKey));
	    if (window.intervalId == null) {
		window.intervalId = [];
	    }
	    for (index = 0; index < window.intervalId.length; index++) {
		clearInterval(window.intervalId[index]);
	    }
	    window.intervalId.push(intervalId);
            var book = document.getElementById("notify-book");
            var bookKey = book.value;
            var chapter = document.getElementById("notify-chapter");
            var chapterKey = chapter.value;
            var verse = document.getElementById("notify-verse");
            var verseKey = verse.value;
            var translation = document.getElementById("notify-translation");
            var translationKey = translation.value;
	    if (translationKey == "ESV") {
		App.Router.router.transitionTo('verse', bookKey+chapterKey+":"+verseKey);
	    } else {
		App.Router.router.transitionTo('verse', bookKey+chapterKey+":"+verseKey+"?translation="+translationKey);
	    }
	},
	stop: function() {
	    if (Notify.needsPermission) {
		Notify.requestPermission(onPermissionGranted, onPermissionDenied);
	    } else {
		if (window.intervalId == null) {
		    window.intervalId = [];
		}
		for (index = 0; index < window.intervalId.length; index++) {
		    clearInterval(window.intervalId[index]);
		}
		window.intervalId = [];
	    }
	}
    }
});

App.VerseRoute = Ember.Route.extend({
    afterModel: function(model, transition) {
	var self = this;
	Ember.run.next(function() {
	    var url = self.get('router.url');
	    url = url.replace(/ /g, "");
            console.log("URL", url);
	    var verse = /\/([\d]*[A-Za-z]+)(\d+)\:(\d+).+=([A-Z]+)/;
	    var matches = url.match(verse);
	    if (matches == null) {
		verse = /\/([\d]*[A-Za-z]+)(\d+)\:(\d+)/;
		matches = url.match(verse);
		var translation = "ESV";
	    } else {
		var translation = matches[4];
	    }
	    var book = matches[1];
	    if ($.isNumeric(book[0])) {
		bookCorrected = book[0] + book[1].toUpperCase() + book.slice(2).toLowerCase();
	    } else {
		bookCorrected = book[0].toUpperCase() + book.slice(1).toLowerCase();
	    }
	    var chapter = matches[2];
	    var verseNum = matches[3];
            $("#notify-book").val(bookCorrected);
            $("#notify-chapter").val(chapter);
            $("#notify-verse").val(verseNum);
            $("#notify-translation").val(translation);
	});
    }
});

App.AboutRoute = Ember.Route.extend({
    afterModel: function(model, transition) {
	alert("about");
    }
});
